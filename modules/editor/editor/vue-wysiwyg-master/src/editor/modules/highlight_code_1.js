module.exports = {
	title: "highlight_code_1",
	customAction (utils) {
        const sel = utils.getHTMLOfSelection();

        // wrap selection in custom html tags
        return [
            ["insertHTML", `<pre v-highlightjs><code class="javascript">${sel}</code></pre>`, false],
        ]
    },
	icon: '<i>c1</i>'
}
