module.exports = {
	title: "highlight_code_5",
	customAction (utils) {
        const sel = utils.getHTMLOfSelection();

        // wrap selection in custom html tags
        return [
            ["insertHTML", `<highlight-code lang="javascript">${sel}</highlight-code>`, false],
        ]
    },
	icon: "<i>c5</i>"
}
