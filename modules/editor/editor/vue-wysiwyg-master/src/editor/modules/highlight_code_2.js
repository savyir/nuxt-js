module.exports = {
	title: "highlight_code_2",
	customAction (utils) {
        const sel = utils.getHTMLOfSelection();

        // wrap selection in custom html tags
        return [
            ["insertHTML", `<pre v-highlightjs><code class="javascript">${sel}</code></pre>`, false],
        ]
    },
	icon: "<i>c2</i>"
}
