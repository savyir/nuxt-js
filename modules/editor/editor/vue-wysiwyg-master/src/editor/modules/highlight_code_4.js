module.exports = {
	title: "highlight_code_4",
	customAction (utils) {
        const sel = utils.getHTMLOfSelection();

        // wrap selection in custom html tags
        return [
            ["insertHTML", `<prism-editor code="${sel}" language="js"></prism-editor>`, false],
        ]
    },
	icon: "<i>c4</i>"
}
