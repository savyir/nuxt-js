module.exports = {
	title: "highlight_code_3",
	customAction (utils) {
        const sel = utils.getHTMLOfSelection();

        // wrap selection in custom html tags
        return [
            ["insertHTML", `<vue-code-highlight>${sel}</vue-code-highlight>`, false],
        ]
    },
	icon: "<i>c3</i>"
}
