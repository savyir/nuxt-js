module.exports = {
    locale_name: 'فارسی',
    locale_shorthand: ['fa', 'farsi'],
    // Translations:
    justifyCenter: 'وسط',
    justifyLeft: 'چپ چین',
    justifyRight: 'راست چین',
    bold: 'ضخیم',
    code: 'کد',
    highlightCode: 'کد مخصوص۲',
    highlight: 'کد مخصوص',
    headings: 'تیتر (h1-h6)',
    link: 'لینک',
    image: 'تصویر',
    italic: 'مایل',
    orderedList: 'لیست شماره گذاری شده (1, 2, 3, ...)',
    unorderedList: 'لیست بی شماره',
    removeFormat: 'Remove formatting.\nClears headings, bold, italic, underlined text, etc.',
    separator: null,
    table: 'جدول',
    underline: 'زیرخط'
}