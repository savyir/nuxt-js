import Vue from 'vue'
import VueMeta from 'vue-meta'
import Helper from '~/assets/Helper.js'
import Constants from '~/assets/constants.js'
import { Editor, EditorContent } from 'tiptap'

const _ = require('lodash');
const markdown = require('markdown');
const moment = require('moment-jalaali');

Vue.use(VueMeta);
Vue.use(VueMeta);
Vue.set(Vue.prototype, '_', _);
Vue.set(Vue.prototype, 'markdown', markdown);
Vue.set(Vue.prototype, 'md', markdown);
Vue.set(Vue.prototype, 'Moment', moment);
Vue.set(Vue.prototype, 'M', moment);
Vue.set(Vue.prototype, 'Helper', Helper);
Vue.set(Vue.prototype, 'H', Helper);
Vue.set(Vue.prototype, 'Constants', Constants);
Vue.set(Vue.prototype, 'C', Constants);
