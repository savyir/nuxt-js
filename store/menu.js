export const state = () => ({
  menu: [],
  slugMenu: {}
})

export const mutations = {
  updateMenu(state, data) {
    state.slugMenu = data.slugMenu;
    state.menu = data.menu;
  }
}
