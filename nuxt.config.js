import colors from 'vuetify/es5/util/colors'
import fa from './assets/lang/fa.js'
import webpack from 'webpack'
require('dotenv').config()

export default {
  mode: 'universal',
  /*
  ** Headers of the page
  */
  head: {
    title: process.env.npm_package_name || 'ناکس جی اس به فارسی',
    titleTemplate: '%s',
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: process.env.npm_package_description || ''}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'},
      {rel: 'stylesheet', type: 'text/css', href: 'assets/awesome/css/fontawesome.min.css'},
    ],
    script: [
      {src: '~/assets/awesome/js/fontawesome.min.js'},
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: {color: '#fff'},
  /*
  ** Global CSS
  */
  css: [
    "~assets/variables.scss",
    "~assets/variables.sass",
    "~assets/font",
    "~assets/style/custom",
    "~assets/style/helper.styl",
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    {src: '~/modules/editor/plugin.js', mode: 'client'},
    '~/plugins/utility',
    '~/plugins/vuetify'
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/dotenv',
    '@nuxtjs/vuetify',
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/dotenv',
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    'nuxt-sweetalert2',
    ['nuxt-validate', {
      lang: 'fa',
      nuxti18n: {
        locale: {
          'fa': 'fa'
        }
      }
      // regular vee-validate options
    }]
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {
    baseURL: process.env.API,
    debug: true
  },

  auth: {
    strategies: {
      social: {
        _scheme: 'oauth2',
        authorization_endpoint: 'https://accounts.google.com/o/oauth2/auth',
        userinfo_endpoint: 'https://www.googleapis.com/oauth2/v3/userinfo',
        scope: ['openid', 'profile', 'email'],
        access_type: undefined,
        access_token_endpoint: undefined,
        response_type: 'token',
        token_type: 'Bearer',
        redirect_uri: undefined,
        client_id: 'SET_ME',
        token_key: 'access_token',
        state: 'UNIQUE_AND_NON_GUESSABLE'
      }
    }
  },
  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    rtl: true,
    lang: {
      locales: {fa},
      current: 'fa',
    },
    icons: {
      iconfont: 'mdiSvg' || 'mdi' || 'md' || 'fa' || 'fab' || 'fa4'
    },
    theme: {
      themes: {
        light: {
          primary: '#41b883',
          accent: colors.blue.darken2,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  server:{
	  port: 3000, // default: 3000,
	  host: '0.0.0.0'
  },
  /*
  ** Build configuration
  */
  build: {

    /*
    ** You can extend webpack config here
    */
    postcss: {
      // Add plugin names as key and arguments as value
      // Install them before as dependencies with npm or yarn
      plugins: {
        // Disable a plugin by passing false as value

      },
      preset: {
        // Change the postcss-preset-env settings
        autoprefixer: {
          grid: true
        }
      }
    },
    extend(config, ctx) {
    },
    plugins: [
      new webpack.ProvidePlugin({
        // global modules
        // '$': 'jquery',
        '_': 'lodash'
      })
    ]
  }
}
