'use strict';
const root = './';
const docs = './documents';
const fs = require('fs').promises;
const _ = require('lodash');

const koa = require('koa');
var bodyParser = require('koa-bodyparser');


const app = new koa(),
  router = require('koa-router')();
app.use(bodyParser());

// get all available menus
router.get('/menu', async (ctx) => {
  try {
    let data = await fs.readFile(docs + `/menu.json`);
    let menu = JSON.parse(_.toString(data));
    let slugMenu = deepKeyBy(menu, 'slug');
    return ctx.body = {menu, slugMenu};
  } catch (err) {
    //console.log('catch', {err});
  }
});

function deepKeyBy(arr, key) {
  // console.info({arr, key})
  return _(arr)
    .map(function (o) { // map each object in the array
      return _.mapValues(o, function (v) { // map the properties of the object
        return _.isArray(v) ? deepKeyBy(v, key) : v; // if the property value is an array, run deepKeyBy() on it
      });
    })
    .keyBy(key); // index the object by the key
}

async function getMenuBySlug() {
  let data = await fs.readFile(docs + `/menu.json`);
  let menu = JSON.parse(data);
  return JSON.parse(JSON.stringify(deepKeyBy(menu, 'slug')));
}

router.get('/content/:item', async (ctx) => {
  let item = ctx.params.item;
  let items = item.split('.');
  let path = items.join('/')
  let slugs = await getMenuBySlug();
  let slugPath = items.join('.items.');
  let menuSubItems = _.get(slugs, slugPath, []);
  let result = {};
  for (let key in _.get(menuSubItems, 'items', {})) {
    let obj = _.get(menuSubItems.items, key, {});
    let _path = docs + '/' + path + '/' + key + '.html'
    try {
      let _data = await fs.readFile(_path);
      _.set(result, key, {...obj, data: _.toString(_data)});
    } catch (e) {
      _.set(result, key, {...obj, data: null});
    }
  }
  return ctx.body = (result)
});
router.put('/save/:item', async (ctx) => {
  try {
    let item = ctx.params.item;

    let items = item.split('.');
    let path = docs + '/' + items.join('/') + '.html'
    let html = ctx.request.body.data;
    try {
      let oldData = await fs.readFile(path);
      if (oldData != html) await fs.writeFile(path + 'bk', oldData);
    } catch (e) {
    }
    await fs.writeFile(path, html);
    return ctx.body = ({status: true, path, html})
  } catch (e) {
    return ctx.body = {status: false, e}
  }
});


function getPathFromUrl(url) {
  return url.split("?")[0];
}


app.use(router.routes()).use(router.allowedMethods())

app.listen(3100, () => console.log('running on port 3100'));
