export default {
    API_URL: process.env.BASE_API_URL,
    PER_PAGE: 10,
    TOP_MENU: [
        {title: 'REGISTER', icon: 'user_account', link: '/auth/register'},
        {title: 'LOGIN', icon: 'user_account', link: '/auth/login'},
        {title: 'PANEL', icon: 'user_account', link: '/auth/panel'},
        {
            title: 'ABOUT', icon: 'user_account', link: '/about', subItems: [
                {title: 'PANEL', icon: 'user_account', link: '/auth/panel'},
                {title: 'PANEL', icon: 'user_account', link: '/auth/panel'},
                {title: 'PANEL', icon: 'user_account', link: '/auth/panel'},
                {title: 'PANEL', icon: 'user_account', link: '/auth/panel'},
            ]
        },
    ],
    ADMIN_DRAWER: [
        {
            title: 'VEHICLES', icon: 'local_shipping', link: '/admin/vehicles', subItems: [
                {title: 'VEHICLE_LIST', icon: 'dashboard', link: '/admin/vehicles'},
                {title: 'VEHICLES', icon: 'shop', link: '/driver/orders'},
                {title: 'INVENTORY', icon: 'dashboard', link: '/driver/tokens'},
                {title: 'ACCOUNTING', icon: 'account_circle', link: '/driver/profile/edit'},
            ]
        },
        {title: 'SHIPMENT', icon: 'dashboard', link: '/driver/map'},
        {title: 'ORDERS', icon: 'shop', link: '/driver/orders'},
        {title: 'INVENTORY', icon: 'dashboard', link: '/driver/tokens'},
        {title: 'INSPECT', icon: 'dashboard', link: '/driver/tokens'},
        {title: 'ACCOUNTING', icon: 'account_circle', link: '/driver/profile/edit'},
    ],
    FOOTER_LINKS: [
        {title: 'ABOUT_US', icon: 'contact', link: '/page/about-us'},
        {title: 'LOGS', icon: 'contact', link: '/page/about-us'},
        {title: 'RULES', icon: 'contact', link: '/page/about-us'},
    ],
    ADMIN_DASHBOARD: [
        // {title: 'QUICK_ACCESS', icon: 'contact', link: '/page/about-us'},
        // {title: 'LOGS', icon: 'contact', link: '/page/rules'},
    ],
}
