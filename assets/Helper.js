const moment = require('moment-jalaali');
const constants = require('~/assets/constants.js');
const apiServer = constants.API_URL
const markdown = require("markdown").markdown;

export default {
  apiServer,
  nl2br(str, is_xhtml = true) {
    const breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>'; // Adjust comment to avoid issue on phpjs.org display
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
  },
  faToEn(str) {
    let persianNumbers = [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g],
      arabicNumbers = [/٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g];
    if (typeof str === 'string') {
      for (let i = 0; i < 10; i++) {
        str = str.replace(persianNumbers[i], i).replace(arabicNumbers[i], i);
      }
    }
    return str;
  },
  markdown(md_content, is_xhtml = true) {
    md_content = markdown.toHTML(md_content)
    const breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>'; // Adjust comment to avoid issue on phpjs.org display
    return (md_content + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
  },
  timeDiff(time) {
    let duration = moment.duration(Date.now() - new Date(time).getTime());
    return duration.asHours();
  },
  goToPay(amount, type = 'charge') {
    return '/payment/pay?amount=' + amount + '&type=' + type;
  },
  toJalaali(date, toFormat = 'jYYYY/jM/jD h:mm') {
    var m = moment(date, 'YYYY/M/D h:mm');
    return m.format(toFormat);
  },
  toJalaaliShow(date, toFormat = 'jYYYY/jM/jD') {
    if (date) {
      var m = moment(date, 'YYYY/M/D h:mm');
      return m.format(toFormat);
    } else {
      return 'یک تاریخ انتخاب کنید'
    }
  },
  getPersianMask() {
    return [/۰/g, /۱/g, /۲/g, /۳/g, /۴/g, /۵/g, /۶/g, /۷/g, /۸/g, /۹/g, /٠/g, /١/g, /٢/g, /٣/g, /٤/g, /٥/g, /٦/g, /٧/g, /٨/g, /٩/g];
  },
  numberFormat(num) {
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
  },
  advertFilterToQuery(filter) {
    let query = {}
    if (_.has(filter, 'search')) {
      _.set(query, 'title_contains', filter.search)
      //_.set(query, 'desc_contains', filter.search)
    }
    if (_.has(filter, 'sort')) _.set(query, '_sort', filter.sort)
    if (_.has(filter, 'category')) _.set(query, 'category', filter.category)
    if (_.has(filter, 'sellType')) _.set(query, 'sellType', filter.sellType)
    if (_.has(filter, 'province')) _.set(query, 'province', filter.province)
    if (_.has(filter, 'minPrice')) _.set(query, 'price_gt', filter.minPrice)
    if (_.has(filter, 'maxPrice')) _.set(query, 'price_lt', filter.maxPrice)
    if (_.get(filter, 'instant', false)) _.set(query, 'instant', filter.instant)
    if (_.get(filter, 'verify', false)) _.set(query, 'verify', filter.verify)
    if (_.has(filter, 'color')) {
      query["color.id_in"] = [];
      _.forEach(filter.color, (id, i) => {
        query["color.id_in"].push(id)
      })
    }
    return query;
  }
}
